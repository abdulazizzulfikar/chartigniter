<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ChartMahasiswa extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('ChartMahasiswaModel');
	}

	public function index(){
		$data = [
			'pie_chart_mahasiswa'	=> $this->pie_chart_mahasiswa()
		];
		$this->load->view('chart_mahasiswa/chart_mahasiswa_view',$data);
	}

	public function pie_chart_mahasiswa(){
		$data 			= $this->ChartMahasiswaModel->get_data_mahasiswa();
		$chart_json 	= "
			var pie_mahasiswa = echarts.init(document.getElementById('pie_chart_mahasiswa'));
			option = {
			    tooltip : {
			        show : true
			    },
			    legend: {
			        orient: 'vertical',
			        left: 'left',
			        data: ['".$data['chart_pria']['label']."','".$data['chart_wanita']['label']."']
			    },
			    series : [
			        {
			            name: '".$data['chart_name']."',
			            type: 'pie',
			            radius : '55%',
			            center: ['50%', '60%'],
			            data:[
			                {	value:".$data['chart_pria']['value'].", 
			                	name:'".$data['chart_pria']['label']."'
			                },
			                
			                {
			                	value:".$data['chart_wanita']['value'].", 
			                	name:'".$data['chart_wanita']['label']."'
			                },
			            ],
			            itemStyle: {
			                emphasis: {
			                    shadowBlur: 10,
			                    shadowOffsetX: 0,
			                    shadowColor: 'rgba(0, 0, 0, 0.5)'
			                }
			            }
			        }
			    ]
			};
			pie_mahasiswa.setOption(option);
			console.log(pie_mahasiswa);
		";
		return $chart_json;
	}
}





/* End of file ChartMahasiswaController.php */
/* Location=> ./application/controllers/ChartMahasiswaController.php */