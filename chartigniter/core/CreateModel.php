<?php

	class CreateModel{

		public function data_model($params){
				$data = "";
				$data .= '
<?php
	defined("BASEPATH") OR exit("No direct script access allowed");
	class Chart'.ucfirst($params["table"]).'Model extends CI_Model {

		public function get_data_'.$params['table'].'(){
			$data = [
				"chart_name"	=> "Data '.ucfirst($params['table']).' Berdasarkan '.ucfirst($params['kategori']).'",
';
$data .= '
					"chart_'.strtolower($params['label_name']).'"	=> [
							"label"		=> "'.ucfirst(strtolower($params['label_name'])).'",
							"value"		=> $this->count_'.strtolower($params['label_name']).'()
						],
					#CONFIG
';
					$data .= '
			];

			return $data;
		}
';

$data .= '
		public function count_'.strtolower($params['label_name']).'(){
			$query = $this->db->select("*")
			->from("'.$params['table'].'")
			->where("'.$params['field'].'","'.$params['parameter'].'")
			->get();
			if ($query->num_rows() > 0) {
				return $query->num_rows();
			}
			else{
				return 0;
			}
		}

		#COUNT

}
	';


			if (file_exists("../application/models/Chart".ucfirst($params['table'])."Model.php")) {
				$data_config .= '
					"chart_'.strtolower($params['label_name']).'"	=> [
							"label"		=> "'.strtolower($params['label_name']).'",
							"value"		=> $this->count_'.strtolower($params['label_name']).'()
						],
					#CONFIG
				';

				$path_to_file = "../application/models/Chart".ucfirst($params['table'])."Model.php";
				$file_contents = file_get_contents($path_to_file);
				$file_contents = str_replace("#CONFIG",$data_config,$file_contents);
				file_put_contents($path_to_file,$file_contents);

				$data_count = '
		public function count_'.strtolower($params['label_name']).'(){
			$query = $this->db->select("*")
			->from("'.$params['table'].'")
			->where("'.$params['field'].'","'.$params['parameter'].'")
			->get();
			if ($query->num_rows() > 0) {
				return $query->num_rows();
			}
			else{
				return 0;
			}
		}

		#COUNT
	';
				$path_to_file2 		= "../application/models/Chart".ucfirst($params['table'])."Model.php";
				$file_contents2 	= file_get_contents($path_to_file2);
				$file_contents2 	= str_replace("#COUNT",$data_count,$file_contents2);
				file_put_contents($path_to_file2,$file_contents2);
			}
			elseif(!file_exists("../application/models/Chart".ucfirst($params['table'])."Model.php")){
				$fp = fopen("../application/models/Chart".ucfirst($params['table'])."Model.php", 'w');
				fwrite($fp, $data);
				fclose($fp);
			}
		}

// ============================================================

	}

	$cm = new CreateModel();