<?php require('core/ChartIgniter.php'); ?>
<?php 
	session_start(); 
	// var_dump($_SESSION);
	// session_destroy();
 ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>ChartIgniter Builder</title>
		<link rel="stylesheet" href="assets/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/typicons/2.0.9/typicons.min.css">
	</head>
	<body>
		<h1 class="text-center">Hello World</h1>
		<div class="container-fluid">
			<div class="col-sm-12 col-md-6 col-lg-6">
				<div class="panel panel-default">
					<div class="panel-body">
						<form method="POST">
							<div class="form-group">
								<label>Tabel</label>
								<select name="tabel" id="inputTabel" class="form-control" required="required">
									<?php foreach ($ci->table_list() as $row): ?>
										<option value="<?= $row['table_name'] ?>"><?= $row['table_name'] ?></option>
									<?php endforeach ?>
								</select>
							</div>
							<div class="form-group">
								<label>Kategori Chart</label>
								<input type="text" name="kategori" class="form-control" required>
							</div>
							<div class="form-group">
								<label>Nama Label</label>
								<input type="text" name="label_name" class="form-control" required>
							</div>
							<div class="form-group">
								<label>Field</label>
								<input type="text" name="field" class="form-control" required>
							</div>
							<div class="form-group">
								<label>Parameter</label>
								<input type="text" name="parameter" class="form-control" required>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-lg btn-info" name="simpan">Simpan Data</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
						
		<!-- jQuery -->
		<script src="assets/jquery.min.js"></script>
		<script src="assets/popper.min.js"></script>
		<script src="assets/bootstrap.min.js"></script>
 		<script src="assets/appendgrid.min.js"></script>
	    <pre>
	    	<?php 
				if (isset($_POST['simpan'])) {
					$data = [
						'table'			=> $_POST['tabel'],
						'field'			=> $_POST['field'],
						'parameter'		=> $_POST['parameter'],
						'kategori'		=> $_POST['kategori'],
						'label_name'	=> $_POST['label_name']
					];
					require('core/CreateModel.php');
					var_dump($cm->data_model($data));
						
				}
			?>
	    </pre>
		    

	</body>
</html>
