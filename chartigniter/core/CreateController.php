<?php

	class CreateController{
				public function create_controller($params){
$data_controller = "";
$data_controller .= "
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Chart".ucfirst($params['table'])." extends CI_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('Chart".ucfirst($params['table'])."Model');
		}

		public function index(){
			$data = [
				'pie_chart_".$params['table']."'	=> $this->pie_chart_".$params['table']."()
			];
			$this->load->view('chart_".$params['table']."/chart_".$params['table']."_view',$data);
		}";
$data_controller .= "
		public function pie_chart_".$params['table']."(){
		$data 			= $this->Chart".ucfirst($params['table'])."Model->get_data_".$params['table']."();
		$chart_json 	= '
			var pie_".$params['table']." = echarts.init(document.getElementById('pie_chart_".$params['table']."'));
			option = {
			    tooltip : {
			        show : true
			    },
			    legend: {
			        orient: 'vertical',
			        left: 'left',
			        data: ['
			        \".\$data['chart_".$params['label_name']."']['label'].\"
			        ']
			    },
			    series : [
			        {
			            name: '".$data['chart_name']."',
			            type: 'pie',
			            radius : '55%',
			            center: ['50%', '60%'],
			            data:[
			                {	value:".$data['chart_pria']['value'].", 
			                	name:'".$data['chart_pria']['label']."'
			                },
";



		}
	}