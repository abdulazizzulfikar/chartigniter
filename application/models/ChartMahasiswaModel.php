
<?php
	defined("BASEPATH") OR exit("No direct script access allowed");
	class ChartMahasiswaModel extends CI_Model {

		public function get_data_mahasiswa(){
			$data = [
				"chart_name"	=> "Data Mahasiswa Berdasarkan Gender",

					"chart_pria"	=> [
							"label"		=> "Pria",
							"value"		=> $this->count_pria()
						],
					
					"chart_wanita"	=> [
							"label"		=> "wanita",
							"value"		=> $this->count_wanita()
						],
					#CONFIG
				

			];

			return $data;
		}

		public function count_pria(){
			$query = $this->db->select("*")
			->from("mahasiswa")
			->where("jenis_kelamin","P")
			->get();
			if ($query->num_rows() > 0) {
				return $query->num_rows();
			}
			else{
				return 0;
			}
		}

		
		public function count_wanita(){
			$query = $this->db->select("*")
			->from("mahasiswa")
			->where("jenis_kelamin","W")
			->get();
			if ($query->num_rows() > 0) {
				return $query->num_rows();
			}
			else{
				return 0;
			}
		}

		#COUNT
	

}
	